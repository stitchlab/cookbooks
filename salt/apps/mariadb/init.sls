{% set maria_init = False %}
{% if salt['grains.get']('maria_init', False) == True %}
    {% set maria_init = True %}
{% endif %}

{% if maria_init == False %}
include:
    - apps.maria_init
{% endif %}

mariadb:
    service:
        - enable: True
        - running

/etc/mysql/wordpress.sql:
    file.managed:
        - source: salt://apps/mariadb/wordpress.sql.tmpl
        - makedirs: True
        - template: jinja
        - context:
            user: {{ pillar['service']['mariadb']['user'] }}
            password: {{ pillar['service']['mariadb']['password'] }}