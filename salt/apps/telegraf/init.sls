{% set telegraf_init = False %}
{% if salt['grains.get']('telegraf_init', False) == True %}
    {% set telegraf_init = True %}
{% endif %}

{% if telegraf_init == False %}
telegraf-init-script:
    cmd.script:
        - source: salt://apps/telegraf/init_script.sh

telegraf_init:
    grains:
        - present
        - value: True
        - require:
            - cmd: telegraf-init-script 
{% endif %}

telegraf:
    pkg:
        - installed

    service:
        - enable: True
        - running
        - require:
            - pkg: telegraf
        - watch:
            - file: /etc/telegraf/telegraf.conf

/etc/telegraf/telegraf.conf:
    file.managed:
        - source: salt://apps/telegraf/telegraf.conf.tmpl
        - makedirs: True
        - template: jinja