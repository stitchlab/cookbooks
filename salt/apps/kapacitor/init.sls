{% set influx_init = False %}
{% if salt['grains.get']('influx_init', False) == True %}
    {% set influx_init = True %}
{% endif %}

{% if influx_init == False %}
include:
    - apps.influx_init
{% endif %}

kapacitor:
    pkg:
        - installed

    service:
        - enable: True
        - running
        - require:
            - pkg: kapacitor
        - watch:
            - file: /etc/kapacitor/kapacitor.conf 

/etc/kapacitor/kapacitor.conf:
    file.managed:
        - source: salt://apps/kapacitor/kapacitor.conf.tmpl
        - makedirs: True
        - template: mako
        - context:
            hookurl: {{ pillar['service']['kapacitor']['slack']['hookurl'] }}
            channel: {{ pillar['service']['kapacitor']['slack']['channel'] }}

/data/kapacitor/cpu_idle_alert.tick:
    file.managed:
        - source: salt://apps/kapacitor/cpu_idle_alert.tick
        - makedirs: True

/data/kapacitor/docker_container_cpu_usage_alert.tick:
    file.managed:
        - source: salt://apps/kapacitor/docker_container_cpu_usage_alert.tick
        - makedirs: True
