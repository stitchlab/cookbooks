{% if grains['roles']|length > 1 %}

include:
{% for role in grains['roles'] %}
{% if role in ['kapacitor', 'grafana', 'influxdb', 'mariadb', 'telegraf'] %}
    - apps.{{ role }}
{% endif %}
{% endfor %}

{% endif %}
