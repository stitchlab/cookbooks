maria-init-script:
    cmd.script:
        - source: salt://apps/maria_init/init_script.sh

maria_init:
    grains:
        - present
        - value: True
        - require:
            - cmd: maria-init-script

mariadb-pkgs:
    pkg.installed:
        - pkgs:
            - mariadb-server
            - mariadb-client

mariadb:
    service:
        - enable: True
        - running
        - watch:
            - file: /etc/mysql/my.cnf

/etc/mysql/my.cnf:
    file.managed:
        - source: salt://apps/maria_init/my.cnf.tmpl
        - makedirs: True
        - template: mako
        - context:
            innodb_size: {{ (grains["mem_total"] * 0.6) | round | int }}

/etc/mysql/conf.d/galera.cnf:
    file.managed:
        - source: salt://apps/maria_init/galera.cnf.tmpl
        - makedirs: True
        - mode: 644
        - template: jinja
        - context:
            mariadb: {{ pillar['service']['mariadb'] }}
            my_ip: {{ grains['ip4_interfaces']['eth0'][0] }}
            my_hostname: {{ grains['fqdn'] }}
            
{% if grains['ip4_interfaces']['eth0'][0] == pillar['service']['mariadb']['primary']['ip'] %}
mariadb-service-stop:
    cmd.run:
        - name: systemctl stop mariadb

galera-cluster-enabled:
    cmd.run:
        - name: galera_new_cluster

{% else %}
mariadb-service-restart:
    cmd.run:
        - name: systemctl restart mariadb
            
{% endif %}