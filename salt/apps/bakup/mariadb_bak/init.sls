{% set maria_init = False %}
{% if salt['grains.get']('maria_init', False) == True %}
    {% set maria_init = True %}
{% endif %}

{% if maria_init == False %}
include:
    - apps.maria_init
{% endif %}