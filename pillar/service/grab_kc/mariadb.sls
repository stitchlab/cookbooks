mariadb:
    user: "wordpress"
    password: "wordpress"
    set_cluster: True
    cluster_name: galera_cluster
    primary:
        ip: node01_ip
    secondary:
        ip: node02_ip