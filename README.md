# KC's cookbooks for application installation
install applications thorugh SaltStack.

## Summary
- mariaDB(galera)
- influxDB
- grafana
- kapacitor
- telegraf

## Integration
- Update salt mine.
- Assign roles to the minions.
 ```sh
 $ salt '*' mine.update
 $ salt 'minions' grains.setval service xxxxx
 $ salt 'minions' grains.append roles app
 $ salt 'minions' grains.append roles telegraf
 ```